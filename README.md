## Terrafile

The combination of a mix of concerns with the module source and implementation with a potentially laborious and error prone module upgrade process resulted in the creation of a Terrafile to address these issues.

A Terrafile is simple YAML config that gives you a single, convenient location that lists all your external module dependencies.

```ruby
tf-aws-vpc:
 source:  "git@github.com:ITV/tf-aws-vpc"
 version: "v0.1.1"
tf-aws-iam:
 source:  "git@github.com:ITV/tf-aws-iam"
 version: "v0.1.0"
tf-aws-sg:
 source:  "git@github.com:ITV/tf-aws-sg"
 version: "v1.3.1"
tf-aws-asg:
 source:  "git@github.com:ITV/tf-aws-asg"
 version: "v2.0.0"
```

### Usage

'rake get_modules'
